let input =
  Utils.read_lines ~path:"input"
  |> List.map (fun line ->
         Scanf.sscanf line "%s@)%s" (fun pre post -> (post, pre)))

module StringMap = Map.Make (String)
module StringSet = Set.Make (String)

let rec count_orbits acc orbit_map = function
  | "COM" -> acc
  | corpus -> count_orbits (1 + acc) orbit_map (StringMap.find corpus orbit_map)

let rec full_path acc orbit_map = function
  | "COM" -> acc
  | corpus ->
      full_path (StringSet.add corpus acc) orbit_map
        (StringMap.find corpus orbit_map)

let path_to corpus orbits =
  full_path StringSet.empty orbits (StringMap.find corpus orbits)

let () =
  let orbits_around = input |> List.to_seq |> StringMap.of_seq in

  let orbits_length =
    StringMap.map (count_orbits 1 orbits_around) orbits_around
  in

  let n_orbits = StringMap.fold (fun _ acc k -> acc + k) orbits_length 0 in

  let path_to_you = path_to "YOU" orbits_around in
  let path_to_san = path_to "SAN" orbits_around in

  let n_transfers =
    StringSet.cardinal (StringSet.diff path_to_you path_to_san)
    + StringSet.cardinal (StringSet.diff path_to_san path_to_you)
  in

  Printf.printf "Part 1: %u\n" n_orbits;
  Printf.printf "Part 2: %u\n" n_transfers
