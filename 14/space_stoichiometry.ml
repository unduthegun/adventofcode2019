module ProductMap = Map.Make (String)

let input =
  let process_reagent str =
    Scanf.sscanf str " %i %s" (fun amount reagent -> (reagent, amount))
  in
  let process_components str =
    String.split_on_char ',' str |> List.map process_reagent
  in
  Utils.read_lines ~path:"input"
  |> List.map (fun l ->
         Scanf.sscanf l "%s@=> %i %s" (fun components amount product ->
             (product, (amount, process_components components))))
  |> List.to_seq
  |> ProductMap.of_seq

let cost reactions base product amount =
  let rec loop total_cost silo product amount =
    match (product, amount) with
    | _, 0 -> (total_cost, silo)
    | material, _ when material = base -> (total_cost + amount, silo)
    | _, _ ->
        let available =
          ProductMap.find_opt product silo |> Option.value ~default:0
        in
        let missing = max 0 (amount - available) in

        let quantity, reagents = ProductMap.find product reactions in
        (* Round up the number of reactions *)
        let reactions_needed = (missing + quantity - 1) / quantity in
        let produced = quantity * reactions_needed in
        let excess = max 0 (available + produced - amount) in

        let silo = ProductMap.add product excess silo in

        List.fold_left
          (fun (total, silo) (reagent, amount) ->
            loop total silo reagent (amount * reactions_needed))
          (total_cost, silo) reagents
  in
  let used, _ = loop 0 ProductMap.empty product amount in
  used

let rec bin_search f value low high =
  let mid = (low + high) / 2 in
  let this = f mid in
  if high = low then if this > value then mid - 1 else mid
  else if this > value then bin_search f value low (mid - 1)
  else if this < value then bin_search f value (mid + 1) high
  else mid

let search f value =
  let rec bound_search bound =
    if f bound > value then (bound / 2, bound) else bound_search (bound * 2)
  in
  let low, high = bound_search 1 in
  bin_search f value low high

let () =
  let request = cost input "ORE" "FUEL" in
  let required = request 1 in
  let produced = search request 1000000000000 in

  Printf.printf "Part 1: %i\n" required;
  Printf.printf "Part 2: %i\n" produced
