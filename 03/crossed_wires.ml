module C = Coord

type point = C.t
type segment = point * point * int

let ( +++ ) = C.add
let ( --- ) = C.sub
let ( *** ) = C.mul

let input =
  let to_move c k =
    match c with
    | 'R' -> { C.x = k; y = 0 }
    | 'U' -> { C.x = 0; y = k }
    | 'L' -> { C.x = -k; y = 0 }
    | 'D' -> { C.x = 0; y = -k }
    | _ -> raise Exit
  in

  Utils.read_lines ~path:"input"
  |> List.map (fun line ->
         String.split_on_char ',' line
         |> List.map (fun s -> Scanf.sscanf s "%c%i" to_move))

let step (segments, start, travelled) move : segment list * point * int =
  let fin = start +++ move in
  ((start, fin, travelled) :: segments, fin, travelled + C.norm move)

let walk moves =
  let paths, _end_point, _travelled =
    List.fold_left step ([], C.zero, 0) moves
  in
  paths

let projection a b c d : float option =
  let e = b --- a in
  let f = d --- c in
  let p = { C.x = -e.y; y = e.x } in

  let project a p = (a.C.x * p.C.x) + (a.C.y * p.C.y) in

  let projection = project f p in
  if projection = 0 then None
  else
    let n = Float.of_int (project (C.sub a c) p) in
    let d = Float.of_int projection in
    Some (n /. d)

let intersection (a, b, d1) (c, d, d2) : (point * int) option =
  let ( let* ) = Option.bind in
  let* h1 = projection a b c d in
  let* h2 = projection c d a b in

  let f = d --- c in
  if h1 > 0. && h1 < 1. && h2 > 0. && h2 < 1. then
    let x = Int.of_float (Float.of_int f.x *. h1) in
    let y = Int.of_float (Float.of_int f.y *. h1) in
    let i = c +++ { C.x; y } in
    let delay = C.norm (i --- a) + d1 + (C.norm (i --- c) + d2) in
    Some (i, delay)
  else None

let find_intersections path_a path_b : (point * int) list =
  List.concat_map
    (fun segment_a ->
      List.filter_map (fun segment_b -> intersection segment_a segment_b) path_b)
    path_a

let () =
  let a, b = match input with [ a; b ] -> (a, b) | _ -> raise Exit in

  let paths_a = walk a in
  let paths_b = walk b in

  let crossings = find_intersections paths_a paths_b in

  let closest_to_center =
    List.fold_left
      (fun acc ({ C.x; y }, _) -> min acc (abs x + abs y))
      Int.max_int crossings
  in

  let lowest_delay =
    List.fold_left (fun acc (_, d) -> min acc d) Int.max_int crossings
  in

  Printf.printf "Part 1: %u\n" closest_to_center;
  Printf.printf "Part 2: %u\n" lowest_delay
