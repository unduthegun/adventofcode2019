let input =
  Utils.read_lines ~path:"input"
  |> List.map (fun line -> Scanf.sscanf line "%u" Fun.id)

let fuel_for_module mass = (mass / 3) - 2

let fuel_for_module_overhead mass =
  let rec loop acc mass =
    match fuel_for_module mass with
    | overhead when overhead <= 0 -> acc
    | overhead -> loop (overhead + acc) overhead
  in
  loop 0 mass

let sum = List.fold_left ( + ) 0

let () =
  let requirement = input |> List.map fuel_for_module |> sum in
  let requirement_meta = input |> List.map fuel_for_module_overhead |> sum in

  Printf.printf "Part 1: %u\n" requirement;
  Printf.printf "Part 2: %u\n" requirement_meta
