# Advent of code 2019

Each day has a corresponding directory with its solution.

To build and run the solutions dune is needed.

To run all solutions:

        dune build @run

To run a solution for a day:

        dune build @01/run

There is the additional directory `00` with helper methods to help reading files.
