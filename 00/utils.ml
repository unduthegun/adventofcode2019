let rec read_lines f input () =
  try Seq.Cons (f (input_line input), read_lines f input)
  with _ ->
    close_in input;
    Seq.Nil

let with_input_channel file f = f (open_in file)
let map_lines f file_path = with_input_channel file_path (read_lines f)
let read_lines ~path = map_lines Fun.id path |> List.of_seq
let read_a_line ~path = try with_input_channel path input_line with _ -> ""

let int_rev k n =
  let rec loop rev n = function
    | 0 -> rev
    | k -> loop ((rev * 10) + (n mod 10)) (n / 10) (k - 1)
  in
  loop 0 n k

let int_of_bool b = if b then 1 else 0
let between ~value l h = (value > l && value < h) || (value < l && value > h)
let rec gcd a = function 0 -> a | b -> gcd b (a mod b)

let lcm m n =
  match (m, n) with 0, _ | _, 0 -> 0 | m, n -> abs (m * n) / gcd m n

let range from until =
  let start, upto = if until < from then (until, from) else (from, until) in
  let next = function x when x > upto -> None | x -> Some (x, x + 1) in
  Seq.unfold next start

let min_fst tuple tuple' = if fst tuple > fst tuple' then tuple' else tuple

let concat img start length =
  let result = Buffer.create (3 * length) in
  for i = start to start + length - 1 do
    Buffer.add_utf_8_uchar result (Array.get img i)
  done;
  Buffer.contents result

let render ~width (raw : Uchar.t Seq.t) =
  let flat_image = Array.of_seq raw in
  let image_length = Array.length flat_image in

  let height = Float.(ceil (of_int image_length /. of_int width) |> to_int) in
  let rows = range (height - 1) 0 in

  let matrix_image =
    Seq.map (fun i -> concat flat_image (i * width) width) rows
  in

  matrix_image |> List.of_seq |> String.concat "\n"
