(* Coordinates in a grid *)

type t = { x : int; y : int }

let zero = { x = 0; y = 0 }
let add a b = { x = a.x + b.x; y = a.y + b.y }
let sub a b = { x = a.x - b.x; y = a.y - b.y }
let neg a = { x = -a.x; y = -a.y }
let mul a b = { x = (a.x * b.x) - (a.y * b.y); y = (a.x * b.y) + (a.y * b.x) }
let mul_scalar a k = { x = a.x * k; y = a.y * k }

(* taxicab norm *)
let norm a = abs a.x + abs a.y
